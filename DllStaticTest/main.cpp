#include <Windows.h>
#include <iostream>
#include <vector>
#include "Registry.hpp"
using namespace std;

int main()
{
    LoadLibrary("Dll1.dll");
    cout << "MAIN START" << endl;
    LoadLibrary("Dll2.dll");

    auto& reg = ProcessorRegistry::GetRegisterers();
    vector<Processor*> proc;
    for (auto i = reg.begin(), end = reg.end(); i != end; ++i)
    {
        proc.push_back((*i)->instantiate());
        cout << proc.back()->name() << endl;
    }

    string s;
    cout << "----------------" << endl << "any thing to continue..." << endl;
    cin >> s;

    return 0;
}
