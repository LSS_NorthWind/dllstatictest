#ifndef __REGISTRY_HPP__
#define __REGISTRY_HPP__

#include <vector>
#include <string>
#include <iostream>
using namespace std;

class ProcessorRegistry;

class Processor
{
public:
    const string& name(){ return _name; }

protected:
    Processor(const char* name) : _name(name) {}

private:
    string _name;
};

class ProcessorRegisterer
{
public:
    ProcessorRegisterer(){}
    virtual ~ProcessorRegisterer(){}
    virtual Processor* instantiate() const = 0;
};

template <typename T>
class TypedProcessorRegisterer : public ProcessorRegisterer
{
public:
    TypedProcessorRegisterer()
    {
        cout << "reg init!" << endl;
        ProcessorRegistry::AddRegisterer<T>(this);
    }
    virtual ~TypedProcessorRegisterer(){}
    Processor* instantiate() const override { return new T(); }
};

#ifdef REGAPI
#undef REGAPI
#endif

#ifdef REG_EXPORT
#define REGAPI __declspec(dllexport)
#pragma message("reg export")
#else
#define REGAPI __declspec(dllimport)
#pragma message("reg import")
#endif

class ProcessorRegistry
{
public:
    template <typename T>
    static void AddRegisterer(ProcessorRegisterer* p)
    {
        Registerers.push_back(p);
    }

    static const vector<ProcessorRegisterer*>& GetRegisterers()
    {
        return Registerers;
    }

private:
    REGAPI static vector<ProcessorRegisterer*> Registerers;
};

#ifdef PROCAPI
#undef PROCAPI
#endif

#ifdef PROC_EXPORT
#define PROCAPI __declspec(dllexport)
#pragma message("proc export")
#else
#define PROCAPI __declspec(dllimport)
#pragma message("proc import")
#endif

template <typename T>
class RegisteredProcessor : public Processor
{
public:
    virtual ~RegisteredProcessor(){}
protected:
    RegisteredProcessor(const char* name) : Processor(name) { volatile TypedProcessorRegisterer<T>* x = &ourRegisterer; }
public:
    static TypedProcessorRegisterer<T> ourRegisterer;
};

template <typename T> TypedProcessorRegisterer<T> RegisteredProcessor<T>::ourRegisterer;

#define PROC_DEF(proc_name) class PROCAPI proc_name : public RegisteredProcessor<proc_name> { public: \
        proc_name() : RegisteredProcessor<proc_name>(#proc_name) {}

#define PROC_DEF_END(proc_name) };

#endif
